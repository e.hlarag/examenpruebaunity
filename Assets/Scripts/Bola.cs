using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using TMPro;
using UnityEngine;

public class Bola : MonoBehaviour
{
    // Start is called before the first frame update
    float magnitud = 7f;
    public GameObject PWP;
    public delegate void OnHit();
    public event OnHit onHit;
    bool activo = true;
    void Start()
    {
        colocarpelota();
    }

    void colocarpelota()
    {
        
            float r = Random.Range(45f, -45f);
            this.transform.Rotate(0f, 0f, r);
            this.GetComponent<Rigidbody2D>().velocity = this.transform.up * 7;
        
    }

    // Update is called once per frame
    void Update()
    {
       
        //this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.normalized * magnitud;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Bloque")
        {
            Destroy(collision.gameObject);
            int fifty = Random.Range(1, 3);
            if (fifty == 1)
            {
                int r = Random.Range(1, 5);
                GameObject powerup = Instantiate(PWP);
                powerup.transform.position = collision.transform.position;
                switch (r)
                {
                    case 1:
                        powerup.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    case 2:
                        powerup.GetComponent<SpriteRenderer>().color = Color.blue;
                        break;
                    case 3:
                        powerup.GetComponent<SpriteRenderer>().color = Color.yellow;
                        break;
                    case 4:
                        powerup.GetComponent<SpriteRenderer>().color = Color.green;
                        break;
                }
            }
        }
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "suelo")
        {
            Debug.Log(collision.transform.tag);
            onHit.Invoke(); 
            this.transform.position= new Vector3(0,0, 0);
            colocarpelota();
        }
    }
}
