using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using TMPro;

public class UI : MonoBehaviour
{
    public GameObject pala;
    public GameObject bola;
    // Start is called before the first frame update
    void Start()
    {
        this.bola.GetComponent<Bola>().onHit += actualitzarui;
        this.GetComponent<TextMeshProUGUI>().text = "HP: " + pala.GetComponent<Pala>().hp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    void actualitzarui()
    {
        this.GetComponent<TextMeshProUGUI>().text = "HP: "+pala.GetComponent<Pala>().hp;
    }
}
