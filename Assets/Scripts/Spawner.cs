using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bloques;
    public Bloqueeleccion bloqueeleccion;
    void Start()
    {
        Spawnear();
        print(bloqueeleccion.nbloque);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawnear() 
    {
        float ix = -8.0f;
        float iy = 4f;
        float dx =2.9f;
        float dy =1.2f;

        float x = ix;
        float y = iy;

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                GameObject newGameObjetc = Instantiate(bloques);
                newGameObjetc.transform.position = new Vector3(x, y, 0);
                x += dx;
            }
            x = ix;
            y -= dy;
        }
        


    }
}
