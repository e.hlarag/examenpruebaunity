using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pala : MonoBehaviour
{
    public int speed;
    public GameObject color;
    //public GameObject Bola;
    private Rigidbody2D rb;
    public GameObject bola;
    
    public int hp;
    // Start is called before the first frame update
    void Start()
    {
        hp = 3;
        rb = GetComponent<Rigidbody2D>();
        this.bola.GetComponent<Bola>().onHit += restarvida;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-speed, -rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        if (this.hp <= 0)
        {
            SceneManager.LoadSceneAsync(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "pwp")
        {
            if (collision.GetComponent<SpriteRenderer>().color == Color.red)
            {
                this.color.GetComponent<TextMeshProUGUI>().text = "Vermell";             
            };
            if (collision.GetComponent<SpriteRenderer>().color == Color.blue)
            {
                this.color.GetComponent<TextMeshProUGUI>().text = "Blau";
                int r = Random.Range(2, 5);
                StartCoroutine(bolaquieta(r));
            };
            if (collision.GetComponent<SpriteRenderer>().color == Color.yellow)
            {
                this.color.GetComponent<TextMeshProUGUI>().text = "Groc";
            };
            if (collision.GetComponent<SpriteRenderer>().color == Color.green)
            {
                this.color.GetComponent<TextMeshProUGUI>().text = "Verd";
            };
            Destroy(collision.gameObject);
        }
    }

    IEnumerator bolaquieta(int r)
    {
        this.bola.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        yield return new WaitForSeconds(r);
        this.bola.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

    }

    void restarvida()
    {
        this.hp--;
    }

}
