using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PWP : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -3, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "suelo")
        {
            Destroy(this.gameObject);
        }
    }
}
